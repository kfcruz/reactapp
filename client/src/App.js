import React from 'react';
import CrudApp from './components/CrudApp';

function App() {
  return (
    <>
      <h1>CrudApp</h1>
      <CrudApp />
    </>
  );
}

export default App;

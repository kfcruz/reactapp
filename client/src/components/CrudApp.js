import React, { useState, useEffect } from "react";
import "../App.css";
import CrudForm from "./CrudForm";
import CrudTable from "./CrudTable";
import Axios from "axios";
import Login from "./Login";

const CrudApp = () => {
  const [dataToEdit, setDataToEdit] = useState(null)
  const [session, setSession] = useState(false)

  const [peopleList, setPeopleList] = useState([
    {
      idCustomers: null,
      nameP: "",
      lastNameP: "",
      lastNameM: "",
      gender: "",
      dateBirth: "",
      phone: null
    }
  ])

  useEffect(() => {
    Axios.get('http://localhost:3001/api/get').then((response) => {
      setPeopleList(response.data)
    })
  }, [])

  const logIn = (val) => {
    const { email, password } = val
    Axios.post('http://localhost:3001/api/login', {
      email, password
    }).then((response) => {
      setSession(response.data)
    })
  }

  const logOut = () => {
    setSession(false)
  }

  const signUp = (data) => {
    const { emailNew, passwordNew } = data

    Axios.post('http://localhost:3001/api/insert/user', {
      email: emailNew,
      password: passwordNew,
    })
  }

  const createData = (data) => {
    const { nameP, lastNameP, lastNameM, gender, dateBirth, phone } = data

    Axios.post('http://localhost:3001/api/insert', {
      nameP: nameP,
      lastNameP: lastNameP,
      lastNameM: lastNameM,
      gender: gender,
      dateBirth: dateBirth,
      phone: phone
    })
    window.location.reload()
  };

  const updateData = (data) => {
    Axios.put('http://localhost:3001/api/update', {
      idCustomers: data.idCustomers,
      nameP: data.nameP,
      lastNameP: data.lastNameP,
      lastNameM: data.lastNameM,
      gender: data.gender,
      dateBirth: data.dateBirth,
      phone: data.phone
    })
    window.location.reload()
    setSession(true)
  };

  const deleteData = (id) => {
    let isDelete = window.confirm(`¿Estás seguro de eliminar el registro con el id:'${id}'?`);

    if (isDelete) {
      Axios.delete(`http://localhost:3001/api/delete/${id}`)
    } else {
      return;
    }
    window.location.reload()
    setSession(true)
  };

  return (
    <div>
      {session ? <>
        <CrudForm createData={createData} updateData={updateData} dataToEdit={dataToEdit} setDataToEdit={setDataToEdit} logOut={logOut} />
        <CrudTable data={peopleList} setDataToEdit={setDataToEdit} deleteData={deleteData} /> </>
        :
        <Login logIn={logIn} signUp={signUp} />}
    </div>
  );
};

export default CrudApp;
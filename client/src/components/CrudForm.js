import React, {useState, useEffect} from 'react';

const initialForm = {
    nameP: "",
    lastNameP: "",
    lastNameM: "",
    gender: "",
    dateBirth: null,
    phone: null,
    id: null,
};

const CrudForm = ({createData, updateData, dataToEdit, setDataToEdit, logOut }) => {
    const [form, setForm] = useState(initialForm);

    useEffect(()=> {
        if (dataToEdit) {
            setForm(dataToEdit);
        } else {
            setForm(initialForm);
        }
    }, [dataToEdit])

    const handleChange = (e) => {
        setForm({
            ...form,
            [e.target.name]:e.target.value,
        });
    };
    const handleSubmit = (e) => {
        e.preventDefault();
        if (!(form.nameP || form.lastNameP || form.lastNameM || form.gender || form.dateBirth || form.phone)) {
            alert('Datos incompletos')
            return
        }
        if (form.id === null) {
            createData(form)
        } else {
            updateData(form)
        }
        handleReset();
    };
    const handleReset = (e) => {
        setForm(initialForm);
        setDataToEdit(null);
    };
    const handleLogOut = (e) => {
        logOut()
    }

    return (
        <div>
            <h3>{dataToEdit ? "Editar":"Agregar"}</h3>
            <form onSubmit={handleSubmit}>
                <label>Nombre(s)</label>
                <input type="text" name="nameP" placeholder="Carlos" onChange={handleChange} value={form.nameP} required/>
                <label>Apellido Paterno</label>
                <input type="text" name="lastNameP" placeholder="García" onChange={handleChange} 
                    value={form.lastNameP}/> 
                <label>Apellido Materno</label>
                <input type="text" name="lastNameM" placeholder="García" onChange={handleChange} value={form.lastNameM} required/>
                <label>Género</label> 
                <select name="gender" onChange={handleChange} value={form.gender} required>
                    <option></option>
                    <option>Femenino</option>
                    <option>Masculino</option>
                </select>
                <label>Fecha de Nacimiento</label>
                <input type="date" name="dateBirth" onChange={handleChange} value={form.dateBirth} required/>
                <label>Celular</label>
                <input type="tel" name="phone" placeholder="000-000-0000" onChange={handleChange} value={form.phone} required/>

                <input type="submit" value="Envíar"/>
                <input type="reset" value="Limpiar" onClick={handleReset}/>
            </form>
            <button onClick={handleLogOut}>Cerrar Sesión</button>
        </div>
    );
};

export default CrudForm;
import React from 'react'
import CrudTableRow from './CrudTableRow';

const CrudTable = ({ data, setDataToEdit, deleteData }) => {

    return (
        <div>
            <h3>Lista</h3>
            <table>
                <thead>
                    <tr>
                        <th>Nombre(s)</th>
                        <th>Apellido Paterno</th>
                        <th>Apellido Materno</th>
                        <th>Género</th>
                        <th>Fecha de Nacimiento</th>
                        <th>Número</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    { data.length === 0 ? 
                        <tr>
                            <td colSpan="7">Sin datos</td>
                        </tr>
                    : 
                        data.map(el => <CrudTableRow key={el.idCustomers} el={el} setDataToEdit={setDataToEdit} deleteData={deleteData}/>)
                    }
                </tbody>
            </table>
        </div>
    );
};

export default CrudTable;
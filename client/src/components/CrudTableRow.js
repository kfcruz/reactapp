import React from 'react'

const CrudTableRow = ({ el, setDataToEdit, deleteData}) => {
    let { nameP, lastNameP, lastNameM, gender, dateBirth, phone, idCustomers } = el;

    return (
        <tr>
            <td>{nameP}</td>
            <td>{lastNameP}</td>
            <td>{lastNameM}</td>
            <td>{gender}</td>
            <td>{dateBirth}</td>
            <td>{phone}</td>
            <td><button onClick={() => setDataToEdit(el)}>Editar</button>
                <button onClick={() => deleteData(idCustomers)}>Eliminar</button>
            </td>
        </tr>
    );
};

export default CrudTableRow;
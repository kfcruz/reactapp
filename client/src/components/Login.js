import React, { useState } from 'react'
import '../App.css';

const formSignIn = {
    email: "",
    password: "",
};

const formSignUp = {
    emailNew: "",
    passwordNew: "",
};

const Login = ({ logIn, signUp }) => {
    const [formSignin, setFormSignIn] = useState(formSignIn);
    const [formSignup, setFormSignUp] = useState(formSignUp);

    const handleChange = (e) => {
        setFormSignIn({
            ...formSignin,
            [e.target.name]: e.target.value,
        });
        setFormSignUp({
            ...formSignup,
            [e.target.name]: e.target.value,
        })
    };

    const handleSubmit = (e) => {
        e.preventDefault();
        if (!(formSignin.email || formSignin.password)) {
            alert('Datos incompletos')
            return
        }

        logIn(formSignin);

        handleReset()
    };

    const handleReset = (e) => {
        setFormSignIn(formSignIn);
    };

    const handleRegister = (e) => {
        e.preventDefault();
        if (!(formSignup.emailNew || formSignup.passwordNew)) {
            alert('Datos incompletos')
            return
        }

        signUp(formSignup);
        window.location.reload()
    }



    return (
        <div>
            <h3>Inicio de sesión</h3>
            <form onSubmit={handleSubmit}>
                <label>Correo: </label>
                <input type="email" name="email" placeholder="example@domain.com" onChange={handleChange} value={formSignin.email} required />
                <label>Contraseña: </label>
                <input type="password" name="password" placeholder="********" onChange={handleChange} value={formSignin.password} required />

                <input type="submit" value="Iniciar" />
            </form>
            <br></br>

            <h3>Registro de usuarios</h3>
            <form>
                <label>Correo: </label>
                <input type="email" name="emailNew" placeholder="example@domain.com" onChange={handleChange} value={formSignup.emailNew} required />
                <label>Contraseña: </label>
                <input type="password" name="passwordNew" placeholder="********" onChange={handleChange} value={formSignup.passwordNew} required />

                <button onClick={handleRegister}>Registrar</button>
            </form>
        </div>
    );
};

export default Login;
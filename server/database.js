const mysql = require('mysql')
const { HOST, PORTDB, USERDB, PASSWORD, DATABASE } = process.env

//Configuracion de la base de datos, usando variables del archivo env
const db = mysql.createPool({
    host: `${HOST}`,
    port: `${PORTDB}`,
    user: `${USERDB}`,
    password: `${PASSWORD}`,
    database: `${DATABASE}`,
})

//Conexion a la base de datos
db.getConnection(function(err, connection) {
    if (connection) {//Conexion exitosa
        console.log('DB online')
    } else {//Conexion rechazada
        console.log(err)
    }
}); 

module.exports = db
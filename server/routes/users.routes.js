//Rutas para hacer las peticiones
const { Router } = require('express')
const router = Router()
const db = require('../database')
const bcrypt = require("bcryptjs");
var passwordBD = ""

//Consulta de datos
router.get('/api/get', (req, res) => {
    const sqlSelect = "SELECT * FROM customers;";
    db.query(sqlSelect, (err, result) => {
        (err) ? console.log(err) : res.send(result)
    })
})

//Guardar datos de la persona
router.post('/api/insert', (req, res) => {
    const { nameP, lastNameP, lastNameM, gender, dateBirth, phone } = req.body;

    const sqlInsert =
        "INSERT INTO customers (nameP, lastNameP, lastNameM, gender, dateBirth, phone) VALUES (?,?,?,?,?,?)";
    db.query(sqlInsert, [nameP, lastNameP, lastNameM, gender, dateBirth, phone], (err, result) => {
        (err) ? console.log(err) : console.log(result)
    });
});

//Eliminar persona
router.delete('/api/delete/:id', (req, res) => {
    const id = req.params.id;
    const sqlDelete =
        "DELETE FROM customers WHERE idCustomers = ?";
    db.query(sqlDelete, id, (err, result) => {
        (err) ? console.log(err) : console.log(result)
    })
})

//Editar persona
router.put('/api/update', (req, res) => {
    const { idCustomers, nameP, lastNameP, lastNameM, gender, dateBirth, phone } = req.body;

    const sqlUpdate =
        "UPDATE customers SET nameP = ?, lastNameP = ?, lastNameM = ?, gender = ?, dateBirth = ?, phone = ? WHERE idCustomers = ?";
    db.query(sqlUpdate, [nameP, lastNameP, lastNameM, gender, dateBirth, phone, idCustomers], (err, result) => {
        (err) ? console.log(err) : console.log(result)
    })
})

//Guardar usuario
router.post('/api/insert/user', (req, res) => {
    const emailNew = req.body.email;
    const passwordNew = req.body.password;

    bcrypt.hash(passwordNew, 10, (err, finalPassowrd) => {
        if (err) {
            console.log("Error, no se pudo encriptar la contraseña:", err);
        } else {
            const sqlInsert =
                "INSERT INTO user (email, password) VALUES (?,?)";
            db.query(sqlInsert, [emailNew, finalPassowrd], (err, result) => {
                (err) ? console.log(err) : console.log(result)
            });
        }
    });
});

//Inicio de sesion
router.post('/api/login', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;

    const sqlSelect = "SELECT password FROM user WHERE `" + "email" + "` = '" + email + "'";
    db.query(sqlSelect, (err, result) => {
        if (err) {
            res.send(false)
        } else {
            try {
                passwordBD = result[0].password
                bcrypt.compare(password, passwordBD, (err, resul) => {
                    if (err) {
                        res.send(false)
                    } else {
                        res.send(resul)
                    }
                })
            } catch {
                res.send(false)
            }
        }
    })
});

module.exports = router;
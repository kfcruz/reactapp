const express = require('express')
const morgan = require('morgan')
const cors = require('cors')

// Initializations
const app = express()
app.set('port', process.env.PORT || 3001)

//Middlewares
app.use(cors())
app.use(express.json())
app.use(morgan('dev'))
app.use(express.urlencoded({ extended: true }))

//Routes
app.use(require('./routes/users.routes'))

module.exports = app